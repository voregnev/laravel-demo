FROM php:7.4.3-fpm-buster
RUN apt-get update && apt-get install -y \
  unzip
RUN docker-php-ext-install -j$(nproc) \
  pdo_mysql
COPY --from=composer:1.9.3 /usr/bin/composer /usr/bin

RUN useradd --no-log-init --home-dir /app --user-group app
USER app
WORKDIR /app

COPY --chown=app:app composer.json composer.lock ./
RUN composer install --no-scripts --no-autoloader

COPY --chown=app:app . ./
RUN composer install
